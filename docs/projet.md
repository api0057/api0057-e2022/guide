# Projet

Objectifs pédagogiques :

- [ ] Exploration de données en autonomie
- [ ] Proposition de traitements techniques adéquates et sensés
- [ ] Ie, développer et conforter les compétences acquises pendant l'API

## Rendu

- Soutenance (chill), tout le monde assiste de 16h à 18h!
  Pour chaque groupe :
  - 10 mins de présentation (code / architecture & résultats)
  - 5 mins de question

## Technologies

Pour la base au choix :

- a. PostreSQL / PostGis (4 groupes)
- b. Spark (4 groupes)

Attention, vous n'aurez pas le droit d'utiliser l'autre technologie.

## Questions

1. Répartition de la distance aux supermarchés le plus proche par départements
2. Identifier les déserts médicaux
3. Vous êtes bio-coop, vous devez implanter vos 5 prochains magasins où le faites-vous ?
4. Existe-t-il un lien entre la densité d'habitation et l'altitude ?

## Groupes

- 8 groupes de ~4 personnes, en présentiel
- Choix d'une technologie et d'une question parmi les combinaisons :
  - a1
  - b1
  - a2
  - b2
  - a3
  - b3
  - a4
  - b4

## Bonus

- Faire des cartes avec QGis
