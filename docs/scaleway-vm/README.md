# Scaleway VM

## Règles

- Utiliser les VMs dans le respect de la charte informatique de l'UTC (interdiction formelle de faire du minage de cryptomonnaies, d'héberger des contenus à caractères pornographique, etc.). Toutes infractions donnera lieu à l'application des sanctions disciplianires adéquates.
- Tout le monde est `editor` sur l'organisation Scaleway, SVP:
  - Ne touchez pas aux machines des autres,
  - Ne cramez pas les crédits de tout le monde,
  - Créez des ressources uniquement dans le projet `students`
  - Ne modifiez pas les règles de sécurité qui ont été mises en place (Security Group)

## Création de sa VM

Dans le projet `students`, vous pouvez créer au plus 1 VM de type `GP1XS` (pensez à l'éteindre le soir, la planète vous dit merci). Voici le process à suivre.

1. Choix du projet et de la zone (`PARIS 2` est la zone la plus eco-friendly qui existe, ne nous privons pas !)
   ![](assets/1.zone.png)

2. Choix du type d'instance (`GP1XS` obligatoire pour tout le monde, merci d'avance)
   ![](assets/2.instance_type.png)

3. Choix de l'image de base. Nous vous recommandons **FORTEMENT** (ie, sauf si vous êtes doué⋅e que JBL sur Linux), de choisir l'image "chehabfl..." comme ça vous aurez un debian configuré aux petits oignons.
   ![](assets/3.base_image.png)

4. Choix du nom de l'instance **OBLIGATOIREMENT** votre login UTC, sinon votre VM sera détruite sans sommation.
   ![](assets/4.instance_name.png)

5. Ajoutez votre clef SSH publique de manière adéquate.
   ![](assets/5.ssh_key.png)

6. Et c'est parti !
   ![](assets/6.create.png)

Après votre première connexion (`ssh root@xxx.xx.xx.xxx`), faites

```bash
sudo -u student vim ~student/.ssh/authorized_keys
```

Ajouter une ligne avec votre clef SSH (**Ne retirez pas les 3 premières clefs**), puis sauvegarder.

Ensuite vous pouvez vous connecter à la VM avec l'utilisateur `student` (il est _sudoer_ pour root ne vous inquiétez pas) :

```bash
ssh -A student@xxx.xx.xx.xxx
```
