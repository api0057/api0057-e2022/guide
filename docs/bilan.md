# À retenir API Applied Data Science

## Pratiques générales en info

- Shell

  - ZSH / Oh-my-zsh
  - Configuration dans `.zshrc`

- SSH

  - Génération de clefs SSH
  - SSH d'une machine distante (depuis Windows)
  - Configuration de gitlab
  - Tunnel SSH

- Git

  - Connaissances basiques que le fonctionnement de git
  - Les commandes de bases
  - La notion de pre-commit
  - Faire une PR / MR
  - Découverte du concept de CI (Continuous Integration)

- Python / dev en général

  - Nommage des variables et bonnes pratiques de dev
  - Logging vs Print
  - Usage des `dataclass` (`frozen`)
  - Passage de _secrets_ en variable d'environnement
  - Immutabilité
  - `poetry` pour gérer ses dépendances
  - `black` pour formatter le code
  - `isort` pour trier les imports
  - `mypy` pour faire de l'analyse statique
  - `flake8` / `pylint` pour _linter_ le code

- Tests
  - TDD : Écitures de tests avant l'implémentation
  - Quand écrire des tests ?
  - Coverage d'un programme

## Programmation scientifique et analyse de données

- Programmation efficace

  - Structures de données
  - CPU-Bound vs IO-Bound
  - Le GIL en Python
  - Multi-processing VS Multi-threading
  - Analyse de complexité
  - Utilisation de l'écosystème numérique Python

- Real World Data

  - Pré-traitements / nettoyage des données
  - Interventions de Thibaud et Florent
  - Formats de stockage Avro vs Parquet
  - Chercher de la donnée en Open Data

- Cloud Computing

  - Découverte de l'interface et usage d'un provider Cloud
  - Scaleway :fire:

- PostgreSQL / Postgis

  - Insertion de données depuis Python avec `psycopg`
  - Insertion de données avec `copy`
  - _Prepared Statesment_ avec les paramètres en dehors du SQL
  - Utilisation de `psql`
  - Indexation et influence sur les perfs
  - Examen des plans d'execution
  - Savoir que l'on peut tunner sa configuration PostgreSQL

- Streaming

  - Traitements de données en $O(1)$ en mémoire

- Spark

  - Conecpts généraux de Mapping / Reducion
  - Utilisation de l'API RDD
  - Utilisation de l'API Spark SQL
  - Définition de ses propres UDFs (User Defined Functions)
  - Spark peut être plus lent que un seul thread `Python`

- Gestion de Pipeline

  - Connaissance de l'existance d'Airflow
  - Mise en place de pipeline simples avec `dagster`

- Geographic Data
  - Vous savez que la BAN existe
  - L'ordre des Lat / Long
  - Les différents types de géométries que vous pouvez traiter
  - Connaissance des formats de stockage Geojson et Shapefile
  - Manipulation basique de données géographiques avec `shapely`
  - Les projections de données géographiques (WGS84, Lambert 93, etc.)
  - Une pointe de Qgis pour visualiser les données
